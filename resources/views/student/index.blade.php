@extends('layouts.app')
@section('title', 'Student | home')

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Student's List || 
            <a href="{{route('student.create')}}">Create Student</a>
        </h2>
    
    </div>



    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status')}}
    </div>
    @endif
    
    <div class="card-body"> 
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Father's Name</th>
                <th scope="col">Mother's Name</th>
                <th scope="col">Address</th>
                <th scope="col">Class</th>
                <th scope="col">Department</th>
                <th scope="col">Email</th>
                <th scope="col">Operations</th>
              </tr>
            </thead>
            <tbody>

                @foreach ($students as $student)
                <tr>
                        <th scope="row">{{ $student->id }}</th>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->father_name }}</td>
                        <td>{{ $student->mother_name }}</td>
                        <td>{{ $student->address }}</td>
                        <td>{{ $student->class->title }}</td>
                        <td>{{ $student->department->title }}</td>
                        <td>{{ $student->email }}</td>
                        
                        <td>
                            <a  href="{{route('student.edit', $student->id)}}">Edit</a> || 

                                <form action="{{route('student.delete', $student->id)}}" id="delete-form-{{$student->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                </form>

                            <a href="" onclick="
                            if(confirm('Are you sure to delete this ??'))
                            {
                                event.preventDefault();
                                document.getElementById('delete-form-{{ $student->id }}').submit();
                            } else {
                                event.preventDefault();
                            }">Delete</a>
                        </td>
                      </tr>
                @endforeach
              
              
            </tbody>
          </table>
    </div>
</div>

@endsection