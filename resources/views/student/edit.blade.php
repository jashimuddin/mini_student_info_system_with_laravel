@extends('layouts.app')
@section('title', 'Student | Edit')

@section('content')
    <div class="card">
    <div class="card-header">Edit Student # {{ $student->id }}</div>
        <div class="card-body">
        <form method="POST" action="{{route('student.update', $student->id)}}">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Student Name</label>

                    <div class="col-md-6">
                        <input id="name" type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $student->name }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Student Phone</label>

                    <div class="col-md-6">
                        <input id="phone" type="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $student->phone }}" required autofocus>

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Student Email</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $student->email }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="roll" class="col-md-4 col-form-label text-md-right">Student roll</label>

                    <div class="col-md-6">
                        <input id="roll" type="roll" class="form-control{{ $errors->has('roll') ? ' is-invalid' : '' }}" name="roll" value="{{ $student->roll }}" required autofocus>

                        @if ($errors->has('roll'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('roll') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="reg_id" class="col-md-4 col-form-label text-md-right">Student Reg. ID</label>

                    <div class="col-md-6">
                        <input id="reg_id" type="reg_id" class="form-control{{ $errors->has('reg_id') ? ' is-invalid' : '' }}" 
                        name="reg_id" value="{{ $student->reg_id }}" required autofocus readonly>

                        @if ($errors->has('reg_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('reg_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Department ID</label>

                    <div class="col-md-6">
                        <select name="department_id" id="department_id" class="form-control{{ $errors->has('department_id') ? ' is-invalid' : '' }}" required autofocus>
                            <option value="">Select One</option>
                            @foreach ($departments as $department)
                                
                                <option value="{{ $department->id }}" {{$student->department_id == $department->id ? 'selected' : ''}}>{{ $department->title }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('department_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('department_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label for="class_id" class="col-md-4 col-form-label text-md-right">Class ID</label>
    
                        <div class="col-md-6">
                            <select name="class_id" id="class_id" class="form-control{{ $errors->has('class_id') ? ' is-invalid' : '' }}" required autofocus>
                                <option value="">Select One</option>
                                @foreach ($classes as $class)
                                    
                                    <option value="{{ $class->id }}" {{$student->class_id == $class->id ? 'selected' : ''}}>{{ $class->title }}</option>
                                @endforeach
                            </select>
    
                            @if ($errors->has('class_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('class_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="father_name" class="col-md-4 col-form-label text-md-right">Father Name</label>

                    <div class="col-md-6">
                        <input id="father_name" type="father_name" class="form-control{{ $errors->has('father_name') ? ' is-invalid' : '' }}" name="father_name" value="{{ $student->father_name }}" required autofocus>

                        @if ($errors->has('father_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('father_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="mother_name" class="col-md-4 col-form-label text-md-right">Mother Name</label>

                    <div class="col-md-6">
                        <input id="mother_name" type="mother_name" class="form-control{{ $errors->has('mother_name') ? ' is-invalid' : '' }}" name="mother_name" value="{{ $student->mother_name }}" required autofocus>

                        @if ($errors->has('mother_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mother_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
    
                        <div class="col-md-6">
                            <input id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" 
                            name="address" value="{{ $student->address }}" required autofocus>
    
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="home_number" class="col-md-4 col-form-label text-md-right">Home Number</label>

                    <div class="col-md-6">
                        <input id="home_number" type="home_number" class="form-control{{ $errors->has('home_number') ? ' is-invalid' : '' }}" name="home_number" value="{{ $student->home_number }}" required autofocus>

                        @if ($errors->has('home_number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('home_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
        
@endsection