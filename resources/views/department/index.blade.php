@extends('layouts.app')
@section('title', 'department | home')

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Department List || 
                <a href="{{route('department.create')}}">Create Department</a>
        </h2>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status')}}
        </div>
        @endif
    </div>
    <div class="card-body"> 
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Operations</th>
              </tr>
            </thead>
            <tbody>

                @foreach ($department as $department)
                <tr>
                        <th scope="row">{{ $department->id }}</th>
                        <td>{{ $department->title }}</td>
                        <td>
                            <a  href="{{route('department.edit', $department->id)}}">Edit</a> || 

                                <form action="{{route('department.delete', $department->id)}}" id="delete-form-{{$department->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                </form>

                            <a href="" onclick="
                            if(confirm('Are you sure to delete this ??'))
                            {
                                event.preventDefault();
                                document.getElementById('delete-form-{{ $department->id }}').submit();
                            } else {
                                event.preventDefault();
                            }">Delete</a>
                        </td>
                      </tr>
                @endforeach
              
              
            </tbody>
          </table>
    </div>
</div>

@endsection