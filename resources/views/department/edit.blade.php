@extends('layouts.app')
@section('title', 'department | home')

@section('content')
        <div class="card">
                <div class="card-header text-danger">Department  # {{ $department->id }}</div>
                {{-- show session message  --}}
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status')}}
                        </div>
                    @endif

                <div class="card-body">
                <form method="POST" action="{{route('department.update', $department->id)}}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Department Title</label>
                            <div class="col-md-6">
                                <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $department->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        
@endsection