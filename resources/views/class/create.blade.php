@extends('layouts.app')
@section('title', 'Student | home')

@section('content')
    <div class="card">
        <div class="card-header">Student</div>
           
        <div class="card-body">
        <form method="POST" action="{{route('student.store')}}">
                @csrf

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Add class</label>

                    <div class="col-md-6">
                        <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
        
@endsection