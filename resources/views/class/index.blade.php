@extends('layouts.app')
@section('title', 'department | home')

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Class List || 
            <a href="{{route('class.create')}}">Create New Class</a>
        </h2>
    
    </div>



    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status')}}
    </div>
    @endif
    
    <div class="card-body"> 
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Operations</th>
              </tr>
            </thead>
            <tbody>

                @foreach ($classes as $class)
                <tr>
                        <th scope="row">{{ $class->id }}</th>
                        <td>{{ $class->title }}</td>
                        <td>
                            <a  href="{{route('class.edit', $class->id)}}">Edit</a> || 

                                <form action="{{route('class.delete', $class->id)}}" id="delete-form-{{$class->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                </form>

                            <a href="" onclick="
                            if(confirm('Are you sure to delete this ??'))
                            {
                                event.preventDefault();
                                document.getElementById('delete-form-{{ $class->id }}').submit();
                            } else {
                                event.preventDefault();
                            }">Delete</a>
                        </td>
                      </tr>
                @endforeach
              
              
            </tbody>
          </table>
    </div>
</div>

@endsection