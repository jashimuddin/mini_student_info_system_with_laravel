<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Classes;
use App\Student;

class StudentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $students = Student::all();

        return view('student.index', compact('students'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $departments = Department::all();
        $classes = Classes::all();
        return view('student.create', compact('departments', 'classes'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
         ]);

        // dd($request->all());

        Student::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'roll' => $request->roll,
            'reg_id' => $request->reg_id,
            'department_id' => $request->department_id,
            'class_id' => $request->class_id,
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'address' => $request->address,
            'home_number' => $request->home_number,

        ]);

        return redirect('student')->with('status', 'Student created Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $student = Student::find(1);
        $departments = Department::all();
        $classes = Classes::all();
        return view('student.edit', compact('student', 'departments', 'classes'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $student->name = $request->name;
        $student->phone = $request->phone;
        $student->email =$request->email;
        $student->roll = $request->roll;
        $student->reg_id = $request->reg_id;
        $student->department_id = $request->department_id;
        $student->class_id = $request->class_id;
        $student->father_name = $request->father_name;
        $student->mother_name = $request->mother_name;
        $student->address = $request->address;
        $student->home_number = $request->home_number;

        $student->save();
        return redirect('student')->with('status', 'student updated successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $student = Student::find($id);
        $student->delete();

        return redirect('student');
    }
}
