<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){

// department route
    Route::get('/department', 'DepartmentController@index')->name('department');
    Route::get('/department/create', 'DepartmentController@create')->name('department.create');
    Route::post('department', 'DepartmentController@store')->name('department.store');
    Route::get('department/{id}/edit', 'DepartmentController@edit')->name('department.edit');
    Route::put('department/{id}/update', 'DepartmentController@update')->name('department.update');
    Route::delete('department/{id}/delete', 'DepartmentController@delete')->name('department.delete');

// Class route

    Route::get('class', 'ClassController@index')->name('class');
    Route::get('class/create', 'ClassController@create')->name('class.create');
    Route::post('class', 'ClassController@store')->name('class.store');
    Route::get('class/{id}/edit', 'ClassController@edit')->name('class.edit');
    Route::put('class/{id}/update', 'ClassController@update')->name('class.update');
    Route::delete('class/{id}/delete', 'ClassController@delete')->name('class.delete');

// student route

    Route::get('student', 'StudentController@index')->name('student');
    Route::get('student/create', 'StudentController@create')->name('student.create');
    Route::post('student', 'StudentController@store')->name('student.store');
    Route::get('student/{id}/edit', 'StudentController@edit')->name('student.edit');
    Route::put('student/{id}/update', 'StudentController@update')->name('student.update');
    Route::delete('student/{id}/delete', 'StudentController@delete')->name('student.delete');
});

